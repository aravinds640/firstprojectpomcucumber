/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestSteps {

	ChromeDriver driver;
	@Given("open the Browser")
	public void openTheBrowser() {
	    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    driver = new ChromeDriver();
	    	    
	}

	@Given("Max the Browser")
	public void maxTheBrowser() {
	    driver.manage().window().maximize();
	    
	}

	@Given("Set the Timeout")
	public void setTheTimeout() {
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    
	}

	@Given("Launch the URL")
	public void launchTheURL() {
	    driver.get("http://leaftaps.com/opentaps/");
	    
	}

	@Given("Enter the Username as (.*)")
	public void enterTheUSernameAsDemoSalesManager(String username) {
	    driver.findElementById("username").sendKeys(username);
	    
	}

	@Given("Enter the Password as (.*)")
	public void enterThePasswordAsCrmsfa(String password) {
		driver.findElementById("password").sendKeys(password);
	    
	}

	@Given("Click on Login Button")
	public void clickOnLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
		
	    
	}

	@Given("Click on crm\\/sfa Link")
	public void clickOnCrmSfaLink() {
		driver.findElementByLinkText("CRM/SFA").click();
	    
	}

	@Given("Click on Leads Link")
	public void clickOnLeadsLink() {
		driver.findElementByLinkText("Leads").click();
	    
	}

	@Given("Click on Create Leads link")
	public void clickOnCreateLeadsLink() {
	    
	    driver.findElementByXPath("(//ul[@class='shortcuts']//a)[2]").click();
	}

	@Given("Enter Company name as (.*)")
	public void enterCompanyName(String companyname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(companyname);
		
	    
	}

	@Given("Enter First Name as (.*)")
	public void enterFirstName(String firstname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(firstname);
	    
	}

	@Given("Enter Last Name as (.*)")
	public void enterLastName(String lastname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lastname);
	    
	}

	@When("Click on Crete Leads Button")
	public void clickOnCreteLeadsButton() {
	    driver.findElementByName("submitButton").click();
	    
	}

	@Then("Verify Create Leads First Name as (.*)")
	public void verifyCreateLeadsFirstName(String firstname) {
	    System.out.println(firstname);
	    
	}

}
*/