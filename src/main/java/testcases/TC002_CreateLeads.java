package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLeads extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLeads";
		testDescription = "CreateLeads";
		authors = "Aravind";
		category = "smoke";
		dataSheetName = "TC002";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password,String companyname,String firstname,String lastname) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickcrmsfalink()
		.clickLeadsLink()
		.clickCreateLeadsLink()
		.entercompanyname(companyname)
		.enterfirstname(firstname)
		.enterlastname(lastname)
		.clickCreateLeadbutton()
		.verifyFirstName(firstname);
		
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
}
