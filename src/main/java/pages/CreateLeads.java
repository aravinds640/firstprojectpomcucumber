package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeads extends ProjectMethods{

	public CreateLeads() {
		PageFactory.initElements(driver, this);
	}

	//@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit")
	//WebElement eleLogout;
	@FindBy(id = "createLeadForm_companyName") 
	WebElement elecompname;
	@FindBy(id = "createLeadForm_firstName") 
	WebElement elefirstname;
	@FindBy(id = "createLeadForm_lastName") 
	WebElement elelastname;
	@FindBy(name = "submitButton") 
	WebElement elecreateleadbutton;
	
	@Given("Enter Company name as (.*)")
	public CreateLeads entercompanyname(String data) {		
		type(elecompname, data);
		return this;
	}
	@Given("Enter First Name as (.*)")
	public CreateLeads enterfirstname(String data) {		
		type(elefirstname, data);
		return this;
	}
	@Given("Enter Last Name as (.*)")
	public CreateLeads enterlastname(String data) {		
		type(elelastname, data);
		return this;
	}
	@When("Click on Crete Leads Button")
	public ViewLeads clickCreateLeadbutton() {		
		click(elecreateleadbutton);
		return new ViewLeads();
	}
	}









