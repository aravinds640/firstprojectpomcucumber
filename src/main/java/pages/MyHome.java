package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods{

	public MyHome() {
		PageFactory.initElements(driver, this);
	}

	//@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit")
	//WebElement eleLogout;
	@FindBy(linkText = "Leads") 
	WebElement eleleadslink;
	

	public MyLeads clickLeadsLink() {		
		click(eleleadslink);
		return new MyLeads();
	}
	
	}









